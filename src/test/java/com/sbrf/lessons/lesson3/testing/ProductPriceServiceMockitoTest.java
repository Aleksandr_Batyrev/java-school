package com.sbrf.lessons.lesson3.testing;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ProductPriceServiceMockitoTest {

    @Mock
    private ProductDiscountService discountServiceMock;

    private ProductPriceService priceService;


    @Before
    public void setUpClass() {
        priceService = new ProductPriceServiceImpl(discountServiceMock);
    }

    @Test
    public void testFootwearPrice() {
        Product product = new Product("Кроссовки", ProductCategory.FOOTWEAR, 8000d);
        Mockito.when(discountServiceMock.getDiscount(product)).thenReturn(30d);
        double price = priceService.getPrice(product);
        Assert.assertEquals(5600d, price, 0.000001d);
    }

    @Test
    public void testOuterwearPrice() {
        Product product = new Product("Шуба", ProductCategory.OUTERWEAR, 60_000d);
        Mockito.when(discountServiceMock.getDiscount(product)).thenReturn(15d);
        double price = priceService.getPrice(product);
        Assert.assertEquals(51000d, price, 0.000001d);
    }

    @Test(expected = GetProductPriceException.class)
    public void testGetPriceForNullableProduct() {
        double price = priceService.getPrice(null);
        Assert.assertEquals(51000d, price, 0.000001d);
    }
}
