package com.sbrf.lessons.lesson3.testing;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProductPriceServiceTest {

    private ProductDiscountService discountService;
    private ProductPriceService priceService;


    @Before
    public void setUpClass() {
        discountService = new ProductDiscountServiceImpl();
        priceService = new ProductPriceServiceImpl(discountService);
    }

    @Test
    public void testFootwearPrice() {
        Product product = new Product("Кроссовки", ProductCategory.FOOTWEAR, 8000d);
        double price = priceService.getPrice(product);
        Assert.assertEquals(5600d, price, 0.000001d);
    }

    @Test
    public void testOuterwearPrice() {
        Product product = new Product("Шуба", ProductCategory.OUTERWEAR, 60_000d);
        double price = priceService.getPrice(product);
        Assert.assertEquals(51000d, price, 0.000001d);
    }

    @Test(expected = GetProductPriceException.class)
    public void testGetPriceForNullableProduct() {
        double price = priceService.getPrice(null);
        Assert.assertEquals(51000d, price, 0.000001d);
    }
}
