package com.sbrf.lessons.lesson6;

import java.util.Date;

public class Person {

    // не должно быть пустым, не более 32 символов
    private String name;
    // не должно быть пустым, не более 32 символов
    private String surname;
    // может быть пустым, не более 32 символов
    private String middleName;
    // не должно быть null
    private Date birthdayDate;
    // только положительные значения
    private int age;
    // не менее 6 символов
    private String password;

    public Person(String name, String surname, String middleName, Date birthdayDate, int age, String password) {
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.birthdayDate = birthdayDate;
        this.age = age;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Date getBirthdayDate() {
        return birthdayDate;
    }

    public int getAge() {
        return age;
    }

    public String getPassword() {
        return password;
    }
}
