package com.sbrf.lessons.lesson14.udp_example;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UdpServer {

    public static void main(String[] args) throws IOException {
        try (DatagramSocket socket = new DatagramSocket(9999)) {
            byte[] buffer = new byte[1024];
            String command = "";
            do {
                DatagramPacket datagram = new DatagramPacket(buffer, buffer.length);
                socket.receive(datagram);
                command = new String(datagram.getData(), 0, datagram.getLength());
                System.out.println(command);
            } while (!command.equals("exit"));
        }
    }
}
