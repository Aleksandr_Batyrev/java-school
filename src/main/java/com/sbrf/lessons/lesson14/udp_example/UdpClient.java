package com.sbrf.lessons.lesson14.udp_example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

public class UdpClient {

    public static void main(String[] args) throws IOException {
        InetAddress address = InetAddress.getByName("localhost");
        try (DatagramSocket socket = new DatagramSocket()) {
            BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
            String line;
            do {
                System.out.print("Enter message: ");
                line = console.readLine();
                byte[] bytes = line.getBytes();
                DatagramPacket dp = new DatagramPacket(bytes, bytes.length, address, 9999);
                socket.send(dp);
            } while (!line.equals("exit"));
        }
    }

}
