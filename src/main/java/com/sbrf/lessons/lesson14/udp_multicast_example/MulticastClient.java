package com.sbrf.lessons.lesson14.udp_multicast_example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class MulticastClient {

    public static void main(String[] args) throws IOException {
        InetAddress group = InetAddress.getByName("239.255.255.0");
        try (DatagramSocket socket = new DatagramSocket()) {
            BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
            String line;
            do {
                System.out.print("Enter message: ");
                line = console.readLine();
                byte[] bytes = line.getBytes();
                DatagramPacket dp = new DatagramPacket(bytes, bytes.length, group, 9999);
                socket.send(dp);
            } while (!line.equals("exit"));
        }
    }
}
