package com.sbrf.lessons.lesson14.udp_multicast_example;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MulticastServer {

    public static void main(String[] args) throws IOException {
        try (MulticastSocket socket = new MulticastSocket(9999)) {
            InetAddress multicastAddress = InetAddress.getByName("239.255.255.0");
            socket.joinGroup(multicastAddress);
            byte[] buffer = new byte[1024];
            String command = "";
            do {
                DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length);
                socket.receive(datagramPacket);
                command = new String(datagramPacket.getData(), 0, datagramPacket.getLength());
                System.out.println(command);
            } while (!command.equals("exit"));

            socket.leaveGroup(multicastAddress);
        }
    }
}
