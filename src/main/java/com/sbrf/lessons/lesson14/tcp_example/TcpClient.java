package com.sbrf.lessons.lesson14.tcp_example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class TcpClient {
    public static void main(String[] args) throws IOException {
//        Socket clientSocket = new Socket("localhost", 9999);
        Socket clientSocket = new Socket();
        clientSocket.connect(new InetSocketAddress(InetAddress.getByName("localhost"), 9999));

        InputStream is = clientSocket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        System.out.println(br.readLine());
    }
}
