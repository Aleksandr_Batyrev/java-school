package com.sbrf.lessons.lesson18.example2.engine;

public interface Engine {

    String getEngineParts();
}
