package com.sbrf.lessons.lesson18.example2.vehicle;

import com.sbrf.lessons.lesson18.example2.brakes.ABS;
import com.sbrf.lessons.lesson18.example2.chassis.Chassis;
import com.sbrf.lessons.lesson18.example2.chassis.TruckChassis;
import com.sbrf.lessons.lesson18.example2.climate.AirConditioning;
import com.sbrf.lessons.lesson18.example2.climate.ClimateControlSystem;
import com.sbrf.lessons.lesson18.example2.climate.SeatHeating;
import com.sbrf.lessons.lesson18.example2.engine.Engine;
import com.sbrf.lessons.lesson18.example2.engine.TruckEngine;
import com.sbrf.lessons.lesson18.example2.steering.Steering;
import com.sbrf.lessons.lesson18.example2.wheels.TruckWheels;
import com.sbrf.lessons.lesson18.example2.wheels.Wheels;

public class Truck extends Vehicle {


    public Truck(Chassis chassis, Engine engine, Wheels wheels, Steering steering, ClimateControlSystem climateControlSystem, AirConditioning airConditioning, SeatHeating seatHeating, ABS abs) {
        super(chassis, engine, wheels, steering, climateControlSystem, airConditioning, seatHeating, abs);
    }

    @Override
    public String toString() {
        return "Truck{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                ", steering=" + getSteering() +
                ", climateControlSystem=" + getClimateControlSystem() +
                ", airConditioning=" + getAirConditioning() +
                ", seatHeating=" + getSeatHeating() +
                ", abs=" + getAbs() +
                '}';
    }
}
