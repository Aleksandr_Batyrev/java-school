package com.sbrf.lessons.lesson18.example1.engine;

public class VanEngine {

    private String parts;

    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
