package com.sbrf.lessons.lesson18.example1.vehicle;

import com.sbrf.lessons.lesson18.example1.chassis.VanChassis;
import com.sbrf.lessons.lesson18.example1.engine.VanEngine;
import com.sbrf.lessons.lesson18.example1.wheels.VanWheels;

public class Van {

    private VanChassis chassis;
    private VanEngine engine;
    private VanWheels wheels;

    public Van(VanChassis chassis, VanEngine engine, VanWheels wheels) {
        this.chassis = chassis;
        this.engine = engine;
        this.wheels = wheels;
    }

    public VanChassis getChassis() {
        return chassis;
    }

    public VanEngine getEngine() {
        return engine;
    }

    public VanWheels getWheels() {
        return wheels;
    }

    @Override
    public String toString() {
        return "Van{" +
                "chassis=" + chassis +
                ", engine=" + engine +
                ", wheels=" + wheels +
                '}';
    }
}
