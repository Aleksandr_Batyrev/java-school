package com.sbrf.lessons.lesson18.example1.vehicle;

import com.sbrf.lessons.lesson18.example1.chassis.TruckChassis;
import com.sbrf.lessons.lesson18.example1.engine.TruckEngine;
import com.sbrf.lessons.lesson18.example1.wheels.TruckWheels;

public class Truck {

    private TruckChassis chassis;
    private TruckEngine engine;
    private TruckWheels wheels;

    public Truck(TruckChassis chassis, TruckEngine engine, TruckWheels wheels) {
        this.chassis = chassis;
        this.engine = engine;
        this.wheels = wheels;
    }

    public TruckChassis getChassis() {
        return chassis;
    }

    public TruckEngine getEngine() {
        return engine;
    }

    public TruckWheels getWheels() {
        return wheels;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "chassis=" + chassis +
                ", engine=" + engine +
                ", wheels=" + wheels +
                '}';
    }
}
