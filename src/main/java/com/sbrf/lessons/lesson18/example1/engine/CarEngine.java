package com.sbrf.lessons.lesson18.example1.engine;

public class CarEngine {

    private String parts;

    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "CarEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
