package com.sbrf.lessons.lesson18.example1.engine;

public class TruckEngine {

    private String parts;

    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
