package com.sbrf.lessons.lesson18.example1;

import com.sbrf.lessons.lesson18.example1.chassis.CarChassis;
import com.sbrf.lessons.lesson18.example1.chassis.TruckChassis;
import com.sbrf.lessons.lesson18.example1.chassis.VanChassis;
import com.sbrf.lessons.lesson18.example1.engine.CarEngine;
import com.sbrf.lessons.lesson18.example1.engine.TruckEngine;
import com.sbrf.lessons.lesson18.example1.engine.VanEngine;
import com.sbrf.lessons.lesson18.example1.vehicle.Car;
import com.sbrf.lessons.lesson18.example1.vehicle.Truck;
import com.sbrf.lessons.lesson18.example1.vehicle.Van;
import com.sbrf.lessons.lesson18.example1.wheels.CarWheels;
import com.sbrf.lessons.lesson18.example1.wheels.TruckWheels;
import com.sbrf.lessons.lesson18.example1.wheels.VanWheels;

public class Main {

    public static void main(String[] args) {
        CarChassis carChassis = new CarChassis();
        CarEngine carEngine = new CarEngine();
        CarWheels carWheels = new CarWheels();
        Car car = new Car(carChassis, carEngine, carWheels);
        System.out.println(car);

        VanChassis vanChassis = new VanChassis();
        VanEngine vanEngine = new VanEngine();
        VanWheels vanWheels = new VanWheels();
        Van van = new Van(vanChassis, vanEngine, vanWheels);
        System.out.println(van);

        TruckChassis truckChassis = new TruckChassis();
        TruckEngine truckEngine = new TruckEngine();
        TruckWheels truckWheels = new TruckWheels();
        Truck truck = new Truck(truckChassis, truckEngine, truckWheels);
        System.out.println(truck);
    }


}
