package com.sbrf.lessons.lesson18.example1.vehicle;

import com.sbrf.lessons.lesson18.example1.chassis.CarChassis;
import com.sbrf.lessons.lesson18.example1.engine.CarEngine;
import com.sbrf.lessons.lesson18.example1.wheels.CarWheels;

public class Car {

    private CarChassis chassis;
    private CarEngine engine;
    private CarWheels wheels;

    public Car(CarChassis chassis, CarEngine engine, CarWheels wheels) {
        this.chassis = chassis;
        this.engine = engine;
        this.wheels = wheels;
    }

    public CarChassis getChassis() {
        return chassis;
    }

    public CarEngine getEngine() {
        return engine;
    }

    public CarWheels getWheels() {
        return wheels;
    }

    @Override
    public String toString() {
        return "Car{" +
                "chassis=" + chassis +
                ", engine=" + engine +
                ", wheels=" + wheels +
                '}';
    }
}
