package com.sbrf.lessons.lesson18.example3;

public class Point {

    private String name;


    public Point(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
