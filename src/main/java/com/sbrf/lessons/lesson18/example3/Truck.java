package com.sbrf.lessons.lesson18.example3;

public class Truck {

    private Route route;

    public void go() {
        // some implementation
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }
}
