package com.sbrf.lessons.lesson9.gson_example;

public class Client extends Person {

    private long id;

    public Client(long id, String name, int age) {
        super(name, age);
        this.id = id;
        System.out.println("call Client(long id, String name, int age) constructor");
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                '}';
    }
}
