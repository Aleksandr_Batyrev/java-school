package com.sbrf.lessons.lesson9.gson_example;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Main {

    public static void main(String[] args) {
        Client client = new Client(1, "fullName", 20);

        Gson gson = new GsonBuilder().create();
        String jsonClient = gson.toJson(client);
        System.out.println(jsonClient);

        System.out.println("Start deserialization");
        Client deserClient = gson.fromJson(jsonClient, Client.class);
        System.out.println(deserClient);

    }
}
