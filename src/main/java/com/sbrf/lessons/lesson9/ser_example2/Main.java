package com.sbrf.lessons.lesson9.ser_example2;

import java.io.*;

/**
 * Person не расширяет Serializable. Для сериализации дочерних классов Person требуется дефолтный конструктор в классе Person.
 * При наличии дефолтного конструктора в классе Person сериалзиация выполняется, но все его поля имеют значения по умолчанию.
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileOutputStream fos = new FileOutputStream("ser_personFile2");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        Client client = new Client(2, "fullName", 21);
        oos.writeObject(client);

        System.out.println("Start deserialization");
        FileInputStream fis = new FileInputStream("ser_personFile2");
        ObjectInputStream ios = new ObjectInputStream(fis);
        System.out.println(ios.readObject());
    }
}
