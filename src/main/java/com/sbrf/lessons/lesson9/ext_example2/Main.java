package com.sbrf.lessons.lesson9.ext_example2;

import java.io.*;

/**
 * Дочерний класс не наследует Externalizable
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileOutputStream fos = new FileOutputStream("ext_personFile2");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        Client client = new Client(2, "fullName", 21);
        oos.writeObject(client);

        System.out.println("Start deserialization");
        FileInputStream fis = new FileInputStream("ext_personFile2");
        ObjectInputStream ios = new ObjectInputStream(fis);
        System.out.println(ios.readObject());
    }
}
