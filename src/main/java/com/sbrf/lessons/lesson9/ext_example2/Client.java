package com.sbrf.lessons.lesson9.ext_example2;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Client extends Person implements Externalizable {

    private long id;


    public Client() {
        System.out.println("call Client() constructor");
    }

    public Client(long id, String name, int age) {
        super(name, age);
        this.id = id;
        System.out.println("call Client(long id, String name, int age) constructor");
    }

    public long getId() {
        return id;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeLong(this.id);

    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.id = in.readLong();
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                '}';
    }
}
