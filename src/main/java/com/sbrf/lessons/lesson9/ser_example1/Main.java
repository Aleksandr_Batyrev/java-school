package com.sbrf.lessons.lesson9.ser_example1;

import java.io.*;

/**
 * Person расширяет Serializable. Для сериализации дочерних классов не требуется никаких дополнительных действий
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileOutputStream fos = new FileOutputStream("ser_personFile1");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        Client client = new Client(1, "fullName", 20);
        oos.writeObject(client);

        System.out.println("Start deserialization");
        FileInputStream fis = new FileInputStream("ser_personFile1");
        ObjectInputStream ios = new ObjectInputStream(fis);
        System.out.println(ios.readObject());
    }
}
