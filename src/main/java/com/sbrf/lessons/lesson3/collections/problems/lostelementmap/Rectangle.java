package com.sbrf.lessons.lesson3.collections.problems.lostelementmap;

public class Rectangle {

    private int width;
    private int length;

    public Rectangle(int width, int length) {
        this.width = width;
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle square = (Rectangle) o;

        if (width != square.width) return false;
        return length == square.length;
    }

    @Override
    public int hashCode() {
        return 31 * width + length;
    }
}
