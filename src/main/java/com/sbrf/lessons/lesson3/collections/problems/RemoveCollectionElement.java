package com.sbrf.lessons.lesson3.collections.problems;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RemoveCollectionElement {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("str1");
        list.add("str2");
        list.add("str3");
        list.add("str4");
        System.out.println(list);

//        for (String str : list) {
//            if (str.equals("str2")) {
//                list.remove(str);
//            }
//        }
//
//        // 1)
//        List<String> filteredList = new ArrayList<>();
//        for (String str : list) {
//            if (!str.equals("str2")) {
//                filteredList.add(str);
//            }
//        }
//        System.out.println(filteredList);
//
        // 2)


//        for (Iterator<String> it = list.iterator(); it.hasNext();) {
//            String str = it.next();
//            if (str.equals("str2")) {
//                it.remove();
//            }
//        }
//        System.out.println(list);
//
        //3)
        list.removeIf(str -> str.equals("str2"));
        System.out.println(list);

    }
}
