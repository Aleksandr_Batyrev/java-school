package com.sbrf.lessons.lesson3.collections.problems;

import java.util.*;

public class NPECollection {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add(null);
        list.add("12asdasd");
        list.add("zxczxc");

//        for (String s : list) {
//            // NullPointerException
//            if (s.contains("+")) {
//                // some actions
//            }
//        }

        Set<String> set = new HashSet<>();
        set.add(null);
        set.add("12asdasd");
        set.add("zxczxc");

//        for (String s : set) {
//            // NullPointerException
//            if (s.contains("+")) {
//                // some actions
//            }
//        }

//        Map<String, String> map = new HashMap<>();
//        map.put(null, "someValue1");
//        map.put("someKey2", "someValue1");
//        map.put("someKey3", null);
//
//        for (Map.Entry<String, String> entry : map.entrySet()) {
//            // NullPointerException
//            String key = entry.getKey();
//            String value = entry.getValue();
//            if (key.equals("someValue3")) {
//                // some actions
//                String newValue = value.toUpperCase();
//            }
//        }

        // В TreeMap и TreeSet нельзя добавить null
        TreeSet<String> treeSet = new TreeSet<>();
        treeSet.add(null);
    }
}
