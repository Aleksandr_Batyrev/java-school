package com.sbrf.lessons.lesson3.collections.comparator;

import java.util.Comparator;

public class PersonComparator implements Comparator<Person> {


    @Override
    public int compare(Person o1, Person o2) {
        int nameCompareResult = o1.getName().compareTo(o2.getName());
        if (nameCompareResult == 0) {
            return Integer.compare(o1.getAge(), o2.getAge());
        }

        return nameCompareResult;
    }
}
