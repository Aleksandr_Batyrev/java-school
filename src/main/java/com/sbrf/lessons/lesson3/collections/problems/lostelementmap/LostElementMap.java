package com.sbrf.lessons.lesson3.collections.problems.lostelementmap;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class LostElementMap {

    public static void main(String[] args) {
        Map<Rectangle, String> map = new HashMap<>();

        Rectangle rectangle = new Rectangle(2, 3);
        map.put(rectangle, "my rectangle");
        System.out.println(map.get(rectangle));

        rectangle.setLength(7);
        System.out.println(map.get(rectangle));

        LinkedList<String> ll = new LinkedList<>();
        ll.add("1");
        ll.add("2");
        ll.add("lesson3");
        for (String s : ll) {
            System.out.println(s);
        }
        System.out.println(ll.offer("4"));
        for (String s : ll) {
            System.out.println(s);
        }

        ll.peek();
        ll.element();
        ll.getFirst();
        ll.addFirst("7");
        System.out.println("---");
        for (String s : ll) {
            System.out.println(s);
        }
    }
}

