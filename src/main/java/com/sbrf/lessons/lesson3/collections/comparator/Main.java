package com.sbrf.lessons.lesson3.collections.comparator;



import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {


//        TreeSet<Person> set = new TreeSet<>();
        TreeSet<Person> set = new TreeSet<>(new PersonComparator());

        Person changedPerson = new Person("Пётр", 10);
        set.add(changedPerson);
        set.add(new Person("Пётр", 50));
        set.add(new Person("Пётр", 19));
        set.add(new Person("Фёдор", 52));
        set.add(new Person("Фёдор", 42));
        set.add(new Person("Фёдор", 33));
        set.add(new Person("Семён", 35));
        set.add(new Person("Семён", 21));
        set.add(new Person("Семён", 3));
        set.add(new Person("Виктор", 20));
        set.add(new Person("Виктор", 30));
        set.add(new Person("Виктор", 40));



        for (Person person : set) {
            System.out.println(person);
        }

        changedPerson.setAge(100);

        System.out.println("------------");
        for (Person person : set) {
            System.out.println(person);
        }



    }
}
