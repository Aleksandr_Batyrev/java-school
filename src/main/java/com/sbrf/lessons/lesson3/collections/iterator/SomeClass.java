package com.sbrf.lessons.lesson3.collections.iterator;

public class SomeClass {

    private String name;
    private int someint;

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + someint;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null ) {
            return false;
        }

        if (!(obj instanceof SomeClass)){
            return false;
        }

        SomeClass someObj = (SomeClass) obj;
        return this.name.equals(someObj.getName());
    }

    public String getName() {
        return name;
    }

    public int getSomneint() {
        return someint;
    }
}
