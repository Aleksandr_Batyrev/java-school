package com.sbrf.lessons.lesson3.collections.parametrized;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // до версии Java 1.5
        List list = new ArrayList();
        list.add(new String("string1"));
        list.add(new String("string2"));
        list.add(new String("string3"));

        list.add(new Integer(2));



//        list.add(new Integer(1_000));
//        list.add(new Long(100_000));


    }
}
