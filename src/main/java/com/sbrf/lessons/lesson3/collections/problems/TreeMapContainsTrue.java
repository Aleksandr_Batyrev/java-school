package com.sbrf.lessons.lesson3.collections.problems;

import java.util.TreeMap;

public class TreeMapContainsTrue {

    public static void main(String[] args) {
        TreeMap<Integer, String> map = new TreeMap<>();
        Integer someInt1 = new Integer(1000);
        Integer someInt2 = new Integer(1000);
        map.put(someInt1, new String("someValue"));

        System.out.println(map.containsKey(someInt2));
    }
}

