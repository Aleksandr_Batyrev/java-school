package com.sbrf.lessons.lesson3.testing;

public interface ProductPriceService {

    double getPrice(Product product);
}
