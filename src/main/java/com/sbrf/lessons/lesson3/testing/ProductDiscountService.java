package com.sbrf.lessons.lesson3.testing;

public interface ProductDiscountService {

    double getDiscount(Product product);
}
