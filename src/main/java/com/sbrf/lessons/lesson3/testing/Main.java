package com.sbrf.lessons.lesson3.testing;

public class Main {

    public static void main(String[] args) {
        ProductDiscountService discountService = new ProductDiscountServiceImpl();
        ProductPriceService priceService = new ProductPriceServiceImpl(discountService);

        Product product = new Product("Adidas", ProductCategory.FOOTWEAR, 3000d);
        System.out.println(priceService.getPrice(product));
    }
}
