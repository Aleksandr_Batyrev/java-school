package com.sbrf.lessons.lesson3.testing;

public class ProductDiscountServiceImpl implements ProductDiscountService {


    @Override
    public double getDiscount(Product product) {
        if (product == null) {
            throw new IllegalArgumentException("Аргумент метода ProductDiscountServiceImpl#getDiscount не может быть null");
        }

        switch (product.getCategory()) {
            case FOOTWEAR: {
                return 20d;
            }
            case OUTERWEAR: {
                return 15d;
            }
            case UNDERWEAR: {
                return 10d;
            }
            default:
                return 0d;
        }

    }
}
