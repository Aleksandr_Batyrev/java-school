package com.sbrf.lessons.lesson3.testing;

public class ProductPriceServiceImpl implements ProductPriceService {

    private ProductDiscountService discountService;


    public ProductPriceServiceImpl(ProductDiscountService discountService) {
        this.discountService = discountService;
    }

    @Override
    public double getPrice(Product product) {
        try {
            double discount = discountService.getDiscount(product);
            return product.getPrice() - (product.getPrice() * discount) / 100d;
        } catch (Exception ex) {
            throw new GetProductPriceException("Во время расчета цены продукта возникла ошибка.", ex);
        }
    }
}
