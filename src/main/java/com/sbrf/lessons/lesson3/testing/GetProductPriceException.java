package com.sbrf.lessons.lesson3.testing;

public class GetProductPriceException extends RuntimeException {

    public GetProductPriceException(String message, Throwable cause) {
        super(message, cause);
    }
}
