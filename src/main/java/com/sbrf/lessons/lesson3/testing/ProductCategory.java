package com.sbrf.lessons.lesson3.testing;

public enum ProductCategory {

    FOOTWEAR,
    OUTERWEAR,
    UNDERWEAR,
    AUTO_PARTS
}
